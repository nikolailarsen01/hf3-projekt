<?php
error_reporting(E_ALL);
ini_set('display_errors', True);
require_once("{$_SERVER['DOCUMENT_ROOT']}/router.php");
require_once "vendor/autoload.php";

// Website
// HTML
get('/', 'views/main.html');
get('/login', 'views/login.html');
get('/products', 'views/products.html');

// PHP
post('/login', 'models/Login.html');


// API
// Categories
get('/api/categories', 'api/Category.php');
get('/api/category/$id', 'api/Category.php');
post('/api/category', 'api/Category.php');
put('/api/category', 'api/Category.php');
delete('/api/category/$id', 'api/Category.php');

// Manufacturers
get('/api/manufacturer/$id', 'api/Manufacturer.php');
get('/api/manufacturers', 'api/Manufacturer.php');
post('/api/manufacturer', 'api/Manufacturer.php');
put('/api/manufacturer', 'api/Manufacturer.php');
delete('/api/manufacturer/$id', 'api/Manufacturer.php');

// Products
get('/api/product/$id', 'api/Product.php');
get('/api/products', 'api/Product.php');

// CustomerUser
post('/api/customer_user/create', 'api/CustomerUser.php');
post('/api/customer_user/login', 'api/CustomerUser.php');

// Tokens
post('/api/token', 'api/Token.php');

//404 FEJL
//Hvis der er api i sti navnet
if(strpos(strtolower($_SERVER['REQUEST_URI']), 'api') !== false)
{
    any('/404','api/404.php');
}
else {
    any('/404','views/404.php');
}
