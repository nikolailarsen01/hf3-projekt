<?php
// cli-config.php
require_once "classes/ConnectionManager.php";
$cm = new ConnectionManager();
return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($cm->getEntityManager());
