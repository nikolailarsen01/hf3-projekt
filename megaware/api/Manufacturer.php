<?php
header("Content-Type: application/json");
include_once('handlers/ManufacturerHandler.php');
include_once('api/API.php');
$api = new API();
$man = new ManufacturerHandler();
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $result = $man->create(file_get_contents('php://input'));
    switch ($result)
    {
        case "Created":
            $api->response(201, NULL);
            break;
        case "Bad Request":
            $api->response(400, NULL);
            break;
        case "Conflict":
            $api->response(409, "Row exist");
            break;
        default:
            $api->response(404, NULL);
    } 
}
else if($_SERVER['REQUEST_METHOD'] == 'PUT')
{
    $result = $man->update(file_get_contents('php://input'));
    switch ($result)
    {
        case "OK":
            $api->response(200, "Updated");
            break;
        case "Bad Request":
            $api->response(400, NULL);
            break;
        case "Conflict":
            $api->response(409, "Row exist");
            break;
        default:
            $api->response(404, NULL);
    }  
}
else if ($_SERVER['REQUEST_METHOD'] === 'GET')
{
    if(strpos($_SERVER['REQUEST_URI'], 'manufacturers') == true)
    {
        $result = $man->readAll();
        if(!json_decode($result))   
        {
            error_log($result);
            $api->response(500, NULL);
        }
        else
        {
            $api->response(200, $result);
        } 
    }
    else if(isset ($id))
    {
        $result = $man->readOne($id);
        if(json_decode($result))   
        {
            $api->response(200, $result);
        } 
        else
        {
            switch ($result)
            {
                case "Bad Request":
                    $api->response(400, NULL);
                    break;
                case "Not Found":
                    $api->response(404, NULL);
                    break;
                default:
                    $api->response(404, NULL);
            }
        }
    }
    else 
    {
        $api->response(400, NULL);
    }
}
else if ($_SERVER['REQUEST_METHOD'] === 'DELETE')
{
    if(isset ($id))
    {
        $result = $man->delete($id);

        switch ($result)
        {
            case "OK":
                $api->response(200, "Deleted");
                break;
            case "Bad Request":
                $api->response(400, NULL);
                break;
            case "No Content":
                $api->response(204, NULL);
                break;
            default:
                $api->response(404, NULL);
        }
    }
    else 
    {
        $api->response(400, NULL);
    }
}