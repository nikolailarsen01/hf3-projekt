<?php
class API {
    
    
    public function response($status, $data)
    {
        try{
        header("HTTP/1.1 ".$status);
        
        if(!is_null($data) && json_decode($data))
        {
            $json = json_decode($data);
            $json_response = json_encode($json, JSON_PRETTY_PRINT);
        }
        else
        {
            if(is_null($data))
            {
                switch ($status)
                {
                    case "400":
                        $data = "Check that JSON is correct";
                        break;
                    case "404":
                        $data = "No row found";
                        break;
                    default;
                }
            }
            
            $response['data']=$data;
            $json_response = json_encode($response, JSON_PRETTY_PRINT);
        }
        echo $json_response;
        } catch (TypeError $ex) {
            //Ikke en JSON streng men array
            $response = implode(" ", $data);
            $json_response = json_encode($response, JSON_PRETTY_PRINT);
            echo $json_response;
        }
        
    }
}
