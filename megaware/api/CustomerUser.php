<?php
include_once 'api/API.php';
include_once('handlers/CustomerUserHandler.php');
include_once('api/API.php');
$api = new API();
$user = new CustomerUserHandler();

if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
    if(strpos($_SERVER['REQUEST_URI'], 'create') == true)
    {
        
        $result = $user->create(file_get_contents('php://input'));
        switch ($result)
        {
            case "Created":
                $api->response(201, NULL);
                break;
            case "Bad Request":
                $api->response(400, NULL);
                break;
            case "Conflict":
                $api->response(409, "Row exist");
                break;
            default:
                $api->response(404, NULL);
        } 
    }
    else if(strpos($_SERVER['REQUEST_URI'], 'login') == true)
    {
        
        $result = $user->login(file_get_contents('php://input'));
        
        if(json_decode($result))
        {
            $api->response(200, $result);
        }
        else
        {
            switch ($result)
            {
                case "Bad Request":
                    $api->response(400, NULL);
                    break;
                case "Forbidden":
                    $api->response(403, "Wrong password or username");
                    break;
                default:
                    $api->response(404, NULL);
            } 
        } 
    }
    else 
    {
        $api->response(404, NULL);
    }
}
else 
{
    $api->response(400, NULL);
}