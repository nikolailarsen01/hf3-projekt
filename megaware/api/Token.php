<?php
include_once 'api/API.php';
include_once 'models/CustomerUserTokenModel.php';

$tokmod = new CustomerUserTokenModel();
$api = new API();
if ($_SERVER['REQUEST_METHOD'] === 'POST')
{        
    $result = $tokmod->create(file_get_contents('php://input'));
    
    switch ($result)
    {
        case "Created":
            $api->response(201, NULL);
            break;
        case "Bad Request":
            $api->response(400, NULL);
            break;
        case "Conflict":
            $api->response(409, "Row exist");
            break;
        case "Forbidden":
            $api->response(403, NULL);
            break;
        default:
            $api->response(404, NULL);
    } 
}
else 
{
    $api->response(400, NULL);
}