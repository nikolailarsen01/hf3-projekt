<?php

use Doctrine\ORM\Mapping as ORM;
require_once 'classes/ConnectionManager.php';
require_once 'classes/ITable.php';
require_once 'CustomerUser.php';

/**
 * @ORM\Entity
 * @ORM\Table(name="customers") 
 */

class Customer implements ITable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @var int
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length="64")
     * @var string 
     */
    private $customer_firstname;
    
    /**
     * @ORM\Column(type="string", length="64" )
     * @var string 
     */
    private $customer_lastname;
    
    /**
     * @ORM\Column(type="string", length="64" )
     * @var string 
     */
    private $customer_email;
    
    /**
     * @ORM\Column(type="string", length="32" )
     * @var string 
     */
    private $telephone_number;
    
    /**
     * @ORM\Column(type="string", length="128" )
     * @var string 
     */
    private $company_name;
    
    /**
     * @ORM\Column(type="datetimetz")
     * @var datetime 
     */
    private $created_datetime;
    
    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    private $deleted;
       
    /**
     * @ORM\OneToOne(targetEntity="CustomerUser", mappedBy="customer")
     */
    private $customerUser;
    
    public function __construct() 
    {

    }
    public function toJson()
    {
        return '{ "id": '.$this->id.''
                . ', "customer_firstname": "'.$this->customer_firstname.'"'
                . ', "customer_lastname": "'.$this->customer_lastname.'"'
                . ', "customer_email": "'.$this->customer_email.'"'
                . ', "telephone_number": "'.$this->telephone_number.'"'
                . ', "company_name": "'.$this->company_name.'"'
                . ', "created_datetime": "'.$this->created_datetime->format("c").'"'
                . ', "deleted": "'.$this->deleted.'" }';
    }
     
    public function setId($id)
    {
        $this->id = $id;
    }
    
    public function setName($name)
    {
        $this->name = $name;
    }
  
    public function addUser(CustomerUser $user)
    {
        $this->customerUser = $user;
    }
}

