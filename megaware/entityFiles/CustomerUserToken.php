<?php

use Doctrine\ORM\Mapping as ORM;
require_once 'classes/ConnectionManager.php';
require_once 'classes/ITable.php';
require_once 'CustomerUser.php';

/**
 * @ORM\Entity
 * @ORM\Table(name="customer_user_tokens") 
 */

class CustomerUserToken implements ITable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @var int
     */
    private $id;
    /**
     * @ORM\OneToOne(targetEntity="CustomerUser", inversedBy="customerUserToken")
     */
    private $customerUser;
    
    /**
     * @ORM\Column(type="string", length="255")
     * @var string
     */
    private $token;
    
    /**
     * @ORM\Column(type="datetimetz")
     * @var datetime
     */
    private $expires;
    
    public function __construct() 
    {

    }
    
    public function toJson()
    {
        return array("id" => $this->id, "name" => $this->name);
    }
     
    public function setId($id)
    {
        $this->id = $id;
    }
    
    public function setToken($token)
    {
        $this->token = $token;
    }
    
    public function setExpire($expire)
    {
        $this->expires = $expire;
    }
    
    public function setCustomerUser(CustomerUser $user)
    {
        $user->addToken($this);
        $this->customerUser = $user;
    }
}

