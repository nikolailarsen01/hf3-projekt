<?php

use Doctrine\ORM\Mapping as ORM;
require_once 'classes/ConnectionManager.php';
require_once 'classes/ITable.php';
require_once 'CustomerUserToken.php';
require_once 'Customer.php';

/**
 * @ORM\Entity
 * @ORM\Table(name="customer_users", uniqueConstraints={
 *          @ORM\UniqueConstraint(name="username_idx", columns={"username"})}) 
 */

class CustomerUser implements ITable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @var int
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length="64")
     * 
     * @var string 
     */
    private $username;
    
    /**
     * @ORM\Column(type="string", length="255")
     * @var string
     */
    private $password;
    
    /**
     * @ORM\OneToOne(targetEntity="CustomerUserToken", mappedBy="customerUser")
     */
    private $customerUserToken;
    
    /**
     * @ORM\OneToOne(targetEntity="Customer", inversedBy="customerUser")
     */
    private $customer;
    
    public function __construct() 
    {

    }
    public function toJson()
    {
        $json = '{ "id": '.$this->id.''
                . ', "username": "'.$this->username.'"'
                . ', "password": "'.$this->password.'"'
                . ', "Customer" : '.$this->customer->toJson().' }';
        return $json;
    }
     
    public function setId($id)
    {
        $this->id = $id;
    }
    
    public function setUsername($username)
    {
        $this->username = $username;
    }
  
    public function setPassword($password)
    {
        $this->password = $password;
    }
    public function getPassword()
    {
        return $this->password;
    }
    
    public function addCustomer(Customer $cust)
    {
        $this->customer = $cust;
    }
    
    public function addToken(CustomerUserToken $token)
    {
        $this->customerUserToken = $token;
    }
}

