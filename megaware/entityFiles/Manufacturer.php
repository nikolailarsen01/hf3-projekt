<?php

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
require_once 'classes/ConnectionManager.php';
require_once 'classes/ITable.php';
require_once 'Product.php';

/**
 * @ORM\Entity
 * @ORM\Table(name="manufacturers", uniqueConstraints={
 *          @ORM\UniqueConstraint(name="manufacturers_idx", columns={"name"})}) ) ) 
 */

class Manufacturer implements ITable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer",)
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @var int
     */
    private $id;
    /**
     * @ORM\Column(type="string", length="64")
     * @var string
     */
    private $name;
    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="manufacturer")
     * @var products[] An ArrayCollection of Product objects.
     */
    private $products;
    
    public function __construct() 
    {
        $this->products = new ArrayCollection();
    }
    public function toJson()
    {
        return '{ "id": '.$this->id.', "name": "'.$this->name.'" }';
    }
     
    public function setId($id)
    {
        $this->id = $id;
    }
    
    public function setName($name)
    {
        $this->name = $name;
    }
  
    public function addProduct(Product $product)
    {
        $this->products[] = $product;
    }
}

