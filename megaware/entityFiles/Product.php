<?php

use Doctrine\ORM\Mapping as ORM;
require_once 'Manufacturer.php';
require_once 'Category.php';
require_once 'classes/ITable.php';

/**
 * @ORM\Entity
 * @ORM\Table(name="products") 
 */

class Product implements ITable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length="128")
     */
    private $name;
    /**
     * @ORM\Column(type="string", length="1024")
     */
    private $description;
    /**
     * @ORM\Column(type="decimal")
     */
    private $price;
    /**
     * @ORM\ManyToOne(targetEntity="\Manufacturer", inversedBy="products")
     * @ORM\JoinColumn(name="manufacturer_id", referencedColumnName="id")
     */
    private $manufacturer;
    
    /**
     * @ORM\ManyToOne(targetEntity="\Category", inversedBy="products")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;
    
    public function toJson()
    {
        return '{ "id": '.$this->id.', "name": "'.$this->name.'", "description": "'.$this->description.'", "price": '.$this->price.','
                . ' "Manufacturer": '. $this->manufacturer->toJson() . ','
                . ' "Category": '. $this->category->toJson(). ' }';
    }

    public function setManufacturer(Manufacturer $manufacturer)
    {
        $manufacturer->addProduct($this);
        $this->manufacturer = $manufacturer;
    }
    
    public function setCategory(Category $category)
    {
        $category->addProduct($this);
        $this->category = $category;
    }
}
