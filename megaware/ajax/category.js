var content = '';

async function getCategories()
{
    content = '';
    const response = await fetch('http://localhost/api/categories');
    const categories = await response.json();
    categories.Categories.forEach(categoryList);
    document.getElementById("categories").innerHTML = content;
}
function categoryList(category) 
{
    content += '<li><a class="dropdown-item" style="cursor:pointer" onclick="getCategoryProducts('+category.id+')">'+ category.name + '</li>';
}

async function getCategoryProducts(id)
{
    content = '';
    const response = await fetch('http://localhost/api/category/' + id);
    const categories = await response.json();
    document.getElementById("content").innerHTML = '<div class="container"><div class="row" id="products"></div></div>';
    if(!Object.keys(categories.Products).length == 0)
    {
        categories.Products.forEach(productList);
        document.getElementById("products").innerHTML = content;  
    }
    else
    {
            content += '<div class="col">';
            content += '<div class="card">';
            content += '<div class="card-body">';
            content += 'The category '+categories.name+' does not have any products yet. \n\
            <br>We work very hard to change that!\n\
            <br>In the meantime, look after products in the other categories.\n\
            <br>Cheers! From the MegaWare team';
            content += "</div></div></div>"; 
            document.getElementById("products").innerHTML = content;
    }
}