var content = '';

async function getLoginPage()
{
    $.get( "/login", function( data ) {
    $( "#content" ).html( data );
    });
}

async function login()
{
    const username = $('#username').val();
    const password = $('#password').val();
    const user = {username, password};
    const json = JSON.stringify(user);
    $.post("/api/customer_user/login", json, 
    //Hvis det det er successfuldt
    function(data)
    {
        $("#loginStatus").html("MegaWare login: Success! Redirecting shortly");
        var loggedIn = '<a class="nav-link text-light button" onclick="logout()">Logout  <i class="bi bi-shield-x"></i></a>';
        $("#logged").html(loggedIn); 
        getFrontPage();
        localStorage.setItem("customerId", data.Customer.id);
    }, "json")  
    //Hvis det fejler
    .fail(function()
    {
        $("#loginStatus").html("MegaWare login: Wrong username or password");
    });
}

async function logout()
{
    var loggedIn = '<a class="nav-link text-light button" onclick="getLoginPage()">Login  <i class="bi bi-shield-lock"></i></a>';
    $("#logged").html(loggedIn);
}