var content = '';
const productHtml = '<div class="container"><div class="row" id="products"></div></div>'
async function getProducts()
{
    content = '';
    const response = await fetch('http://localhost/api/products');
    const products = await response.json();
    products.Products.forEach(productList);
    document.getElementById("content").innerHTML = productHtml;
    document.getElementById("products").innerHTML = content;
}
function productList(product) 
{
    content += '<div class="col">';
    content += '<div class="card">';
    content += '<div class="card-body">';
    content += '<h5 class="card-title">'+ product.name + '</h5>';
    content += '<h6 class="card-subtitle mb-2">'+product.Category.name+'/'+product.Manufacturer.name+'</h6>';
    content += ''+ product.description + '';
    content += '</div>'; 
    content += '<div class="card-footer">';
    content += '<div class="d-flex align-items-center gap-2 d-md-flex justify-content-between">';
    content += '<span class="align-middle">'+product.price+',-</span>';
    content += '<button type="button" class="btn btn-success" onclick="addToCart('+product.id+')">Add to cart</button></div></div></div>';
    content += "</div></div>"; 
}