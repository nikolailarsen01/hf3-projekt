<?php
include_once('orm/ORM.php');
include_once('orm/ITable.php');
include_once('models/CustomerUserModel.php');
use ReallySimpleJWT\Token;

class CustomerUserTokenModel extends ITable
{
    public function __construct() {
        $this->tableName = "customer_user_tokens";
    }
    
    public function create($input)
    {
        try {
            $obj = json_decode($input);
            if(!isset($obj->{'username'})) { return "Bad Request"; }
            if(!isset($obj->{'password'})) { return "Bad Request"; }
            $username = $obj->{'username'};
            $password = $obj->{'password'};
            $custUser = new CustomerUserModel();
            $login = $custUser->checkLogin($input);
            if(!json_decode($login)) { return $login; }
            $objUsr = json_decode($login);
            $expires = time() + 360000;
            $issuer = "localhost";
            $token = Token::create($username, $password, $expires, $issuer);
            $expiresDateTime = new DateTime();
            $expiresDateTime->setTimestamp($expires);
            $orm = new ORM();
            return $orm->create($this->tableName, array("customer_userid", "token", "expires"), array($objUsr->{'id'}, $token, $expiresDateTime->format('c')));
        } catch (TypeError $e) {
            error_log($e);
            return $e;
        } catch (Exception $e) {
            error_log($e);
            return $e;
        }     
    }
    
    public function validate($input)
    {
        try {
            $obj = json_decode($input);
            if(!isset($obj->{'token'})) { return "Bad Request"; }
            $token = $obj->{'token'};
            if(!isset($obj->{'password'})) { return "Bad Request"; }
            $password = $obj->{'password'};
            if(Token::validate($token, $password)) { return "OK"; }
            else { return ""; }            
        } catch (TypeError $e) {
            error_log($e);
            return $e;
        } catch (Exception $e) {
            error_log($e);
            return $e;
        }     
    }
}