<?php
include_once('orm/ORM.php');
include_once('orm/ITable.php');

class CategoryModel extends ITable {
    public string $name;
    
    public function __construct() {
        $this->tableName = "categories";
    }
    public function create($input)
    {
        try {
        $obj = json_decode($input);
        if(!isset($obj->{'name'})) { return "Bad Request"; }
        $name = $obj->{'name'};
        $orm = new ORM();
        return $orm->create($this->tableName, array("name"), array($name));
        } catch (TypeError $e) {
            error_log($e);
            return $e;
        } catch (Exception $e){
            error_log($e);
            return $e;
        }
    }
    public function getCategory($name)
    {
        try {
        $orm = new ORM();
        $result = $orm->select($this->tableName, array("name"), array($name));
        if(is_string($result)) { return $result; }
        else 
        {
            $row = pg_fetch_assoc($result);
            $this->id = $row['id'];
            $this->name = $row['name'];
            return $this->toJson();
        }
       
        } catch (TypeError $e) {
            error_log($e);
            return $e;
        }
    }
    public function getCategories()
    {
        try {
        $orm = new ORM();
        $result = $orm->select($this->tableName);
        $categories = array();
        while($row = pg_fetch_assoc($result))
        {
            $category =  new CategoryModel();
            $category->id = $row['id'];
            $category->name = $row['name'];
            array_push($categories, $category);
        }
        return $categories;     
        } catch (TypeError $e) {
            error_log($e);
            return $e;
        }
    }
    public function update($input)
    {
        try {
        $obj = json_decode($input);
        if(!isset($obj->{'name'})) { return "Bad Request"; }
        if(!isset($obj->{'id'})) { return "Bad Request"; }
        $name = $obj->{'name'};
        $id = $obj->{'id'};
        $orm = new ORM();
        return $orm->update($this->tableName, $id, array("name"), array($name));
        } catch (TypeError $e) {
            error_log($e);
            return $e;
        } catch (Exception $e){
            error_log($e);
            return $e;
        }
    }
    public function delete($id) 
    {
        try {
        $orm = new ORM();   
        return $orm->delete($this->tableName, $id);
        } catch (TypeError $e) {
            error_log($e);
            return $e;
        }
    }
}



