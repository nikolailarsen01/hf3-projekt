<?php
include_once('orm/ORM.php');
include_once('orm/ITable.php');

class CustomerModel extends ITable
{
    private string $customer_firstname;
    private string $customer_lastname;
    private string $customer_email;
    private int $telephone_countrycodeid;
    private string $telephone_number;
    private string $company_name;
    private int $addressid;
    private DateTime $created_timestamp;
    private bool $deleted;
     
    public function __construct() {
        $this->tableName = "customers";
    }
    
    public function create($input)
    {
        try {
        $obj = json_decode($input);
        if(!isset($obj->{'customer_email'})) { return "Bad Request"; }
        if(!isset($obj->{'password'})) { return "Bad Request"; }
        $name = $obj->{'name'};
        $orm = new ORM();
        return $orm->create($this->tableName, array("name"), array($name));
        } catch (TypeError $e) {
            error_log($e);
            return $e;
        } catch (Exception $e){
            error_log($e);
            return $e;
        }
    }
    public function readOne($email)
    {
        try {
        $orm = new ORM();
        $result = $orm->select($this->tableName, array("customer_email"), array($email));
        if(is_string($result)) { return $result; }
        else 
        {
            $row = pg_fetch_assoc($result);
            $this->id = $row['id'];
            $this->customer_firstname = $row['customer_firstname'];
            $this->customer_lastname = $row['customer_lastname'];
            $this->customer_email = $row['customer_email'];
            $this->telephone_countrycodeid = $row['telephone_countrycodeid'];
            $this->telephone_number = $row['telephone_number'];
            $this->company_name = $row['company_name'];
            $this->addressid = $row['addressid'];
            $this->created_timestamp = new DateTime($row['created_timestamp']);
            $this->deleted = $row['deleted'];
            return $this->toJson();
        }
        } catch (TypeError $e) {
            error_log($e);
            return $e;
        }
    }
}
