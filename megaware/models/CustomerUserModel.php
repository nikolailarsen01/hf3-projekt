<?php
include_once('orm/ORM.php');
include_once('orm/ITable.php');
include_once('models/CustomerModel.php');

class CustomerUserModel extends ITable
{
    public int $customerid;
    public string $username;
    public string $passwordHashed;
    public function __construct() {
        $this->tableName = "customer_users";
    }
    
    public function create($input)
    {
        try {
        $obj = json_decode($input);
        if(!isset($obj->{'customer_email'})) { return "Bad Request"; }
        if(!isset($obj->{'username'})) { return "Bad Request"; }
        if(!isset($obj->{'password'})) { return "Bad Request"; }
       
        $customer = new CustomerModel();
        $custJson = $customer->readOne($obj->{'customer_email'});
        $custObj = json_decode($custJson);
        if(is_string($custObj)) { return "Bad Request"; } 
        else 
        {
            $hashed = password_hash($obj->{"password"}, PASSWORD_DEFAULT);
            $username = $obj->{"username"};
            $orm = new ORM();
            return $orm->create($this->tableName, array("customerid", "username", "password"), array($custObj->{"id"}, $username, $hashed));
        }
        } catch (TypeError $e) {
            error_log($e);
            return $e;
        } catch (Exception $e){
            error_log($e);
            return $e;
        }
    }
    public function readOne(string $username)
    {
        try {
        $orm = new ORM();
        $result = $orm->select($this->tableName, array("username"), array($username));
        if(is_string($result)) { return $result; }
        else 
        {
            $row = pg_fetch_assoc($result);
            $this->id = $row['id'];
            $this->customerid = $row['customerid'];
            $this->username = $row['username'];
            $this->passwordHashed = $row['password'];
            return $this->toJson();
        }
        } catch (TypeError $e) {
            error_log($e);
            return $e;
        }
    }
    public function checkLogin($input)
    {
        try {
        $obj = json_decode($input);
        if(!isset($obj->{'username'})) { return "Bad Request"; }
        $username = $obj->{'username'};
        if(!isset($obj->{'password'})) { return "Bad Request"; }
        $password = $obj->{'password'};
        $resultCheck = $this->readOne($username);
        if(!json_decode($resultCheck)) { return $resultCheck; }   
        // Tjekker om password er samme som det i databasen
        if(!password_verify($password, $this->passwordHashed)) { return "Forbidden"; }
        else { return $this->toJson(); } 
        } catch (TypeError $e) {
            error_log($e);
            return $e;
        }
    }
}
