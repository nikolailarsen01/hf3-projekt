<?php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

class ConnectionManager {
    private EntityManager $entityManager;
    
    function __construct() 
    {
        $file = 'additional-files/connection.json';
        $json = file_get_contents($file);
        $obj = json_decode($json);
        $connString = array(
            "host" => $obj->{'hostname'}, 
            "port" => $obj->{'port'},
            "dbname" => $obj->{'dbname'},
            "user" => $obj->{'username'},
            "password" => $obj->{'password'},
            "driver" => "pdo_pgsql");
        
        $paths = array("entityFiles/");
        $isDevMode = true;
        $useSimpleAnnotationReader = false;
        $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, null, null, $useSimpleAnnotationReader);
        $this->entityManager = EntityManager::create($connString, $config);
    }
    public function getEntityManager()
    {
        return $this->entityManager;
    }
}
