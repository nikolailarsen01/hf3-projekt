<?php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$file = 'additional-files/connection.json';
        $json = file_get_contents($file);
        $obj = json_decode($json);
        $connString = array(
            "host" => $obj->{'hostname'}, 
            "port" => $obj->{'port'},
            "dbname" => $obj->{'dbname'},
            "user" => $obj->{'username'},
            "password" => $obj->{'password'},
            "driver" => "pdo_pgsql");
        
        $paths = array("entityFiles/");
        $isDevMode = true;
        $useSimpleAnnotationReader = false;
        $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, null, null, $useSimpleAnnotationReader);
        $entityManager = EntityManager::create($connString, $config);


class ORM {
    private PgSql\Connection $conn;
    public $entityManager;
    
    function __construct() 
    {
        $file = 'additional-files/connection.json';
        $json = file_get_contents($file);
        $obj = json_decode($json);
        $connString = array(
            "host" => $obj->{'hostname'}, 
            "port" => $obj->{'port'},
            "dbname" => $obj->{'dbname'},
            "user" => $obj->{'username'},
            "password" => $obj->{'password'},
            "driver" => "pdo_pgsql");
        
        $paths = array("/path/to/entity-files");
        $isDevMode = true;
        $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
        $this->entityManager = EntityManager::create($connString, $config); 
    }
    public function create(string $table, array $colNames, array $colValues )
    {
        try {
        if(sizeof($colNames) == sizeof($colValues))
        {
            // Tjekker hvis den findes i forvejen
            if($this->checkRows($table, $colNames, $colValues) == "Conflict") { return "Conflict"; }
            $cols = "( ";
            $vals = "( ";
            
            for($i = 0; $i < sizeof($colNames); $i++)
            {
                //Laver en streng med kolenne navne og en streng med $n som bruges
                //af pg_query_params
                if($i == 0)
                {
                    $cols = $cols . "$colNames[$i]";
                    $vals = $vals . "$". $i + 1;
                }
                else
                {
                    $cols = $cols . ", $colNames[$i] ";
                    $vals = $vals . ", $". $i + 1 . " "; 
                }
            }
            $cols = $cols . ")";
            $vals = $vals . ")";
            $query = "INSERT INTO $table $cols VALUES $vals";
            
            $result = pg_query_params($this->conn, $query, $colValues);
            if(!$result)
            {
                error_log('There was an error with the query:' . pg_last_error($this->conn) . " Query: $query. Values: $colValues.");
                return "Bad Request";
            }
            else { return "Created"; }
        } else { 
            error_log("Difference in amount columns and values");
            return "Bad Request"; 
        }
        } catch (Exception $e) {
            error_log($e);
            return $e;
        }
    }
    
    public function select(string $table, array $colNames = array("1"), array $colValues = array("1"))
    {
        //Tjekker input
        if(!(sizeof($colNames) == sizeof($colValues))) {
            error_log("Difference in amount columns and values");
            return "Bad Request";
        } 
        $sets = "";
        for($i = 0; $i < sizeof($colNames); $i++)
        {
            //Laver en streng med kolenne navne og $n som bruges
            //af pg_query_params
            if($i == 0)
            {
                $sets = $sets . "$colNames[$i] = $" . $i + 1;
            }
            else
            {
                $sets = $sets . " AND $colNames[$i] = $" . $i + 1; 
            }
        }  
        $query = "SELECT * FROM $table WHERE $sets";
        $result = pg_query_params($this->conn, $query, $colValues);
        
        if(!$result)
        {
            error_log('There was an error with the query:' . pg_last_error($this->conn));
            return 'Bad Request';
        }
        else 
        {
            if(pg_num_rows($result) == 0)
            {
                error_log("There was 0 matching rows: $query"
                        . "\nLast result output: ". pg_result_error($result));
                return 'No Content';
            }
            else { return $result; }
        }
    }
    
    public function update(string $table, int $id, array $colNames, array $colValues)
    {
       if(sizeof($colNames) == sizeof($colValues))
        {
            // Tjekker hvis den findes i forvejen
            if(!$this->checkRows($table, $colNames, $colValues) == "Conflict") { return "No Content"; }

            $sets = "";
            for($i = 0; $i < sizeof($colNames); $i++)
            {
                //Laver en streng med kolenne navne og $n som bruges
                //af pg_query_params
                if($i == 0)
                {
                    $sets = $sets . "$colNames[$i] = $" . $i + 1;
                }
                else
                {
                    $sets = $sets . ", $colNames[$i] = $" . $i + 1 . " "; 
                }
            }   
            $query = "UPDATE $table SET $sets WHERE id = $id";
            $result = pg_query_params($this->conn, $query, $colValues);
            if(!$result)
            {
                error_log('There was an error with the query:' . pg_last_error($this->conn) . ' Query:' . $query);
                return "Bad Request";
            }
            else { return "OK"; }
        } else { 
            error_log("Difference in amount columns and values");
            return "Bad Request"; 
        } 
    }
    public function delete(string $table, int $id)
    {
        // Tjekker hvis den findes i forvejen
        if($this->checkRows($table, array('id'), array($id)) == "No Content") { return "No Content"; }
        
        $query = "DELETE FROM $table WHERE id = $1";
        $result = pg_query_params($this->conn, $query, array($id));
        if(!$result)
        {
            error_log('There was an error with the query:' . pg_last_error($this->conn) . ' Query:' . $query);
            return "Bad Request";
        }
        else { return "OK"; }
    }
    private function checkRows(string $table, array $colNames, array $colValues)
    {
        $where = "";
        for($i = 0; $i < sizeof($colNames); $i++)
        {
            //Laver en streng med kolenne navne og $n som bruges
            //af pg_query_params
            if($i == 0)
            {
                $where = $where . "$colNames[$i] = $" . $i + 1;
            }
            else
            {
                $where = $where . " AND $colNames[$i] = $" . $i + 1; 
            }
        }      
        $query = "SELECT * FROM $table WHERE $where";
        $result = pg_query_params($this->conn, $query, $colValues);
        if(pg_num_rows($result) == 1)
        {
            return "Conflict";
        }
        else { return "No Content"; }
    }
}
