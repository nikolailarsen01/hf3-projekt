<?php
header("Content-Type: application/json");
include_once('entityFiles/CustomerUser.php');
include_once('classes/ConnectionManager.php');
include_once('CustomerHandler.php');
include_once('api/API.php');

class CustomerUserHandler 
{
    private Doctrine\ORM\EntityManager $em;
    public function __construct()
    {
        $cm = new ConnectionManager();
        $this->em = $cm->getEntityManager();
    }
    
    public function create($input)
    {
        try {
        $obj = json_decode($input);
        if(!isset($obj->{'customer_email'})) { return "Bad Request"; }
        $mail = $obj->{'customer_email'};
        if(!isset($obj->{'username'})) { return "Bad Request"; }
        $username = $obj->{'username'};
        if(!isset($obj->{'password'})) { return "Bad Request"; }
        $hashed = password_hash($obj->{"password"}, PASSWORD_DEFAULT);

        $user = new CustomerUser();
        $cust = $this->em->getRepository("Customer")->findOneBy(array('customer_email' => "$mail"));
        
        $user->addCustomer($cust);
        $user->setUsername($username);
        $user->setPassword($hashed);
        $this->em->persist($user);
        $this->em->flush();
        return "Created";
                   
        } catch (ORMException $ex) {
            return "Bad Request";
        } catch (TypeError $ex) {
            error_log($mail);
            return "Bad Request";
        } catch (PDOException $ex) {
            return "Conflict";
        }
    }
    
    public function readOne($input)
    {
        try {
        if(is_int($input))
        {
            $user = $this->em->find("CustomerUser", $input);
        }
        else 
        {
            $user = $this->em->getRepository("CustomerUser")->findOneBy(array('username' => $input));
        }
        return $user->toJson();
        
        } catch (Exception $ex) {
            
        }
    }
    public function readAll()
    {
        try {
        $mans = $this->em->getRepository("Category")->findAll();
        if(is_null($mans)) { return "Not Found"; }
        $result = '{ "Categories": [ ';
        $length = count($mans);
        $i = 1;
        foreach($mans as $man)
        {
            if($i == $length) { $result = $result . $man->toJson(); }
            else { $result = $result . $man->toJson() . ", "; } 
            $i++;
        }

        if(is_null($result)) { return "Not Found"; }
        return $result . " ] }";
        
        } catch (Exception $ex) {
            
        }
    }
    
    public function update($input)
    {
        try {
        $obj = json_decode($input);
        if(!isset($obj->{'name'})) { return "Bad Request"; }
        if(!isset($obj->{'id'})) { return "Bad Request"; }
        $man = new Manufacturer();
        $man->setId($obj->{'id'});
        $man->setName($obj->{'name'});
        $this->em->merge($man);
        $this->em->flush();
        return "OK";        
        } catch (ORMException $ex) {
            return "Bad Request";
        } catch (Doctrine\ORM\EntityNotFoundException $ex) {
            return "Not Found";
        }
    }
    
    public function delete($input)
    {
        try {
        if(!is_string($input)) { return "Bad Request"; }
        $man = $this->em->find("Manufacturer", $input);
        $this->em->remove($man);
        $this->em->flush();
        return "OK";        
        } catch (ORMException $ex) {
            return "Bad Request";
        } catch (Doctrine\ORM\ORMInvalidArgumentException $ex) {
            return "Not Found";
        }
    }
    
    public function login($input)
    {
        try {
        $obj = json_decode($input);
        if(!isset($obj->{'username'})) { return "Bad Request"; }
        $username = $obj->{'username'};
        if(!isset($obj->{'password'})) { return "Bad Request"; }
        $password = $obj->{'password'};
        $user = $this->em->getRepository("CustomerUser")->findOneBy(array('username' => $username));
        // Tjekker om password er samme som det i databasen
        if(!password_verify($password, $user->getPassword())) { return "Forbidden"; }
        else 
        {
            return $user->toJson();
        } 
        } catch (TypeError $e) {
            error_log($e);
            return $e;
        } catch (Error $e) {
            error_log($e);
            return "Forbidden";
        }
    }
}
