<?php
header("Content-Type: application/json");
include_once('entityFiles/Customer.php');
include_once('entityFiles/CustomerUser.php');
include_once('classes/ConnectionManager.php');
include_once('api/API.php');

class CustomerHandler 
{
    private Doctrine\ORM\EntityManager $em;
    public function __construct()
    {
        $cm = new ConnectionManager();
        $this->em = $cm->getEntityManager();
    }
    
    public function create($input)
    {
        try {
        $obj = json_decode($input);
        if(!isset($obj->{'customer_email'})) { return "Bad Request"; }
        $mail = $obj->{'customer_email'};
        if(!isset($obj->{'username'})) { return "Bad Request"; }
        $username = $obj->{'username'};
        if(!isset($obj->{'password'})) { return "Bad Request"; }
        $password = $obj->{'password'};

        $user = new CustomerUser();
        $user->addCustomer();
        $user->setUsername($username);
        $user->setUsername($password);
        $this->em->persist($user);
        $this->em->flush();
        return "Created";
                   
        } catch (ORMException $ex) {
            return "Bad Request";
        }
    }
    
    public function readOne($input)
    {
        try {
        if(is_int($input))
        {
            $customer = $this->em->find("Customer", $input);
        }
        else 
        {
            $customer = $this->em->getRepository("Customer")->findOneBy(array('customer_email' => $input));
        }
        return $customer->toJson();
        
        } catch (Error $ex) {
            
        }
    }
    public function readAll()
    {
        try {
        $mans = $this->em->getRepository("Category")->findAll();
        if(is_null($mans)) { return "Not Found"; }
        $result = '{ "Categories": [ ';
        $length = count($mans);
        $i = 1;
        foreach($mans as $man)
        {
            if($i == $length) { $result = $result . $man->toJson(); }
            else { $result = $result . $man->toJson() . ", "; } 
            $i++;
        }

        if(is_null($result)) { return "Not Found"; }
        return $result . " ] }";
        
        } catch (Exception $ex) {
            
        }
    }
    
    public function update($input)
    {
        try {
        $obj = json_decode($input);
        if(!isset($obj->{'name'})) { return "Bad Request"; }
        if(!isset($obj->{'id'})) { return "Bad Request"; }
        $man = new Manufacturer();
        $man->setId($obj->{'id'});
        $man->setName($obj->{'name'});
        $this->em->merge($man);
        $this->em->flush();
        return "OK";        
        } catch (ORMException $ex) {
            return "Bad Request";
        } catch (Doctrine\ORM\EntityNotFoundException $ex) {
            return "Not Found";
        }
    }
    
    public function delete($input)
    {
        try {
        if(!is_string($input)) { return "Bad Request"; }
        $man = $this->em->find("Manufacturer", $input);
        $this->em->remove($man);
        $this->em->flush();
        return "OK";        
        } catch (ORMException $ex) {
            return "Bad Request";
        } catch (Doctrine\ORM\ORMInvalidArgumentException $ex) {
            return "Not Found";
        }
    }
    
    public function checkLogin($input)
    {
        try {
        $obj = json_decode($input);
        if(!isset($obj->{'username'})) { return "Bad Request"; }
        $username = $obj->{'username'};
        if(!isset($obj->{'password'})) { return "Bad Request"; }
        $password = $obj->{'password'};
        $resultCheck = $this->readOne($username);
        if(!json_decode($resultCheck)) { return $resultCheck; }   
        // Tjekker om password er samme som det i databasen
        if(!password_verify($password, $this->passwordHashed)) { return "Forbidden"; }
        else { return $this->toJson(); } 
        } catch (TypeError $e) {
            error_log($e);
            return $e;
        }
    }
}
