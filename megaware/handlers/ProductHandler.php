<?php
header("Content-Type: application/json");
include_once('entityFiles/Product.php');
include_once('classes/ConnectionManager.php');
include_once('api/API.php');

class ProductHandler 
{
    private Doctrine\ORM\EntityManager $em;
    public function __construct()
    {
        $cm = new ConnectionManager();
        $this->em = $cm->getEntityManager();
    }
    
    public function create($input)
    {
        try {
        if(!json_decode($input)) { return "Bad Request"; }
        $obj = json_decode($input);
        if(!isset($obj->{'name'})) { return "Bad Request"; }
        $name = $obj->{'name'};

        $man = new Manufacturer();
        $man->setName($name);
        $this->em->persist($man);
        $this->em->flush();
        return "Created";
                   
        } catch (ORMException $ex) {
            return "Bad Request";
        }
    }
    
    public function readOne($input)
    {
        try {
        if(is_string($input))
        {
            $prod = $this->em->find("Product", $input);
            if(is_null($prod)) { return "Not Found"; }
            return $prod->toJson();
        }
        if(!json_decode($input)) { return "Bad Request"; }
        $prod = $this->em->find("Product", $input);
        if(is_null($prod)) { return "Not Found"; }
        return $prod->toArray();
        
        } catch (Exception $ex) {
            
        }
    }
    public function readAll()
    {
        try {
        $prods = $this->em->getRepository("Product")->findAll();
        if(empty($prods)) { return "Not Found"; }
        $result = '{ "Products": [ ';
        $length = count($prods);
        $i = 1;
        foreach($prods as $prod)
        {
            if($i == $length) { $result = $result . $prod->toJson(); }
            else { $result = $result . $prod->toJson() . ", "; } 
            $i++;
        }

        if(is_null($result)) { return "Not Found"; }
        return $result . " ] }";
        
        } catch (Exception $ex) {
            
        }
    }
    
    public function update($input)
    {
        try {
        $obj = json_decode($input);
        if(!isset($obj->{'name'})) { return "Bad Request"; }
        if(!isset($obj->{'id'})) { return "Bad Request"; }
        $man = new Manufacturer();
        $man->setId($obj->{'id'});
        $man->setName($obj->{'name'});
        $this->em->merge($man);
        $this->em->flush();
        return "OK";        
        } catch (ORMException $ex) {
            return "Bad Request";
        } catch (Doctrine\ORM\EntityNotFoundException $ex) {
            return "Not Found";
        }
    }
    
    public function delete($input)
    {
        try {
        if(!is_string($input)) { return "Bad Request"; }
        $man = $this->em->find("Manufacturer", $input);
        $this->em->remove($man);
        $this->em->flush();
        return "OK";        
        } catch (ORMException $ex) {
            return "Bad Request";
        } catch (Doctrine\ORM\ORMInvalidArgumentException $ex) {
            return "Not Found";
        }
    }
    
}
