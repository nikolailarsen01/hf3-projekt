<?php
header("Content-Type: application/json");
include_once('entityFiles/Category.php');
include_once('classes/ConnectionManager.php');
include_once('api/API.php');

class CategoryHandler 
{
    private Doctrine\ORM\EntityManager $em;
    public function __construct()
    {
        $cm = new ConnectionManager();
        $this->em = $cm->getEntityManager();
    }
    
    public function create($input)
    {
        try {
        if(!json_decode($input)) { return "Bad Request"; }
        $obj = json_decode($input);
        if(!isset($obj->{'name'})) { return "Bad Request"; }
        $name = $obj->{'name'};

        $cat = new Category();
        $cat->setName($name);
        $this->em->persist($cat);
        $this->em->flush();
        return "Created";
                   
        } catch (ORMException $ex) {
            return "Bad Request";
        } catch (PDOException $ex) {
            return "Internal Server Error";
        } catch (Exception $ex) {
            return "Conflict";
        }
    }
    
    public function readOne($input)
    {
        try {
        if(is_string($input))
        {
            $cat = $this->em->find("Category", $input);
            if(is_null($cat)) { return "Not Found"; }
            return $cat->toJsonFull();
        }
        if(!json_decode($input)) { return "Bad Request"; }
        $cat = $this->em->find("Category", $input);
        if(is_null($cat)) { return "Not Found"; }
        return $cat->toJsonFull();
        
        } catch (Exception $ex) {
            
        }
    }
    public function readAll()
    {
        try {
        $mans = $this->em->getRepository("Category")->findAll();
        if(is_null($mans)) { return "Not Found"; }
        $result = '{ "Categories": [ ';
        $length = count($mans);
        $i = 1;
        foreach($mans as $man)
        {
            if($i == $length) { $result = $result . $man->toJson(); }
            else { $result = $result . $man->toJson() . ", "; } 
            $i++;
        }

        if(is_null($result)) { return "Not Found"; }
        return $result . " ] }";
        
        } catch (Exception $ex) {
            
        }
    }
    
    public function update($input)
    {
        try {
        $obj = json_decode($input);
        if(!isset($obj->{'name'})) { return "Bad Request"; }
        if(!isset($obj->{'id'})) { return "Bad Request"; }
        $cat = new Category();
        $cat->setId($obj->{'id'});
        $cat->setName($obj->{'name'});
        $this->em->merge($cat);
        $this->em->flush();
        return "OK";        
        } catch (ORMException $ex) {
            return "Bad Request";
        } catch (Doctrine\ORM\EntityNotFoundException $ex) {
            return "Not Found";
        }
    }
    
    public function delete($input)
    {
        try {
        if(!is_string($input)) { return "Bad Request"; }
        $cat = $this->em->find("Category", $input);
        $this->em->remove($cat);
        $this->em->flush();
        return "OK";        
        } catch (ORMException $ex) {
            return "Bad Request";
        } catch (Doctrine\ORM\ORMInvalidArgumentException $ex) {
            return "Not Found";
        }
    }
    
}
